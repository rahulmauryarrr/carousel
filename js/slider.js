$(document).ready(function(){
$('.car-1').owlCarousel({
    loop:true,
    margin:30,
    nav:false,
      useTransform:true,
  cssEase:'ease-out',
    dots:false,
    autoplay:false,
    animateOut: 'slideInRight',
    animateIn: 'slideInLeft',
    slideBy: 2,
    responsive:{
        0:{
            items:2
        },
        900:{
            items:1
        },
        1200:{
            items:3
        },
        
    }
});
var owl = $('.car-1');
owl.owlCarousel();
// Go to the next item
$('.next').click(function() {
    owl.trigger('next.owl.carousel');
})
// Go to the previous item
$('.prev').click(function() {
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    owl.trigger('prev.owl.carousel', [300]);
});
});



function openNav() {
  document.getElementById("myNav").style.width = "100%";
}

function closeNav() {
  document.getElementById("myNav").style.width = "0%";
}




$(document).ready(function(){
$('.car-1').owlCarousel({
    loop:true,
    margin:30,
    nav:false,
    dots:false,
    autoplay:100,
    responsive:{
        0:{
            items:1
        },
        900:{
            items:2
        },
        1200:{
            items:2
        },
    }
});
var owl = $('.car-1');
owl.owlCarousel();
// Go to the next item
$('.next').click(function() {
    owl.trigger('next.owl.carousel');
})
// Go to the previous item
$('.prev').click(function() {
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    owl.trigger('prev.owl.carousel', [300]);
});
});





$(document).ready(function(){
$('.car-2').owlCarousel({
    loop:true,
    dots:false,
    nav:false,
    autoplay:100,
    responsive:{
           0:{
            items:1
        },
        900:{
            items:1
        },
        1200:{
            items:1
        },

    }
});
var owl = $('.car-2');
owl.owlCarousel();
// Go to the next item
$('.next1').click(function() {
    owl.trigger('next.owl.carousel');
})
// Go to the previous item
$('.prev1').click(function() {
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    owl.trigger('prev.owl.carousel', [300]);
});
});
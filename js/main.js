$(document).ready(function(){
$('.autoplay').slick({
  margin:20,
  slidesToShow: 2,
  useTransform:true,
  cssEase:'ease-out',
  slidesToScroll: 2,
  dots:true,
  infinite: true,
  centerMode: true,
  autoplay: true,
  arrows:true,
  autoplaySpeed: 2000,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    ]
});
});